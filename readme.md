# Environments
VMs hold/run all software to automate our business, namely back office processing.

Use [PEP-8](https://www.python.org/dev/peps/pep-0008/#tabs-or-spaces).

## Install
* install [VirtualBox](https://www.virtualbox.org/wiki/Downloads), [Vagrant](https://www.vagrantup.com/downloads.html)
* run Terminal then run
```sh
#needed vagrant plugin 
#@todo: find workaround inside Vagrantfile
vagrant plugin install vagrant-vbguest

git clone git@bitbucket.org:jayfranco/dev-env.git
vagrant up
```

if desired, test forwarding
```sh
vagrant ssh
ssh -T git@github.com
```

## Schedule in Windows' Task Scheduler
C:\Python37\python.exe "p:\eCommerce\Planning\Automation\Tier 1.py"
every Monday before work

* configure to [send email if fails](https://superuser.com/questions/249103/make-windows-task-scheduler-alert-me-on-fail/249127)

## Reference
* [inspiration](https://github.com/kevthehermit/VolUtility/blob/master/extra/vagrant/Vagrantfile)

## Todo
* configure [flake8](http://flake8.pycqa.org/en/latest/)
* ensure post-commit hook on repo?
* use a [pipeline](https://bitbucket.org/jayfranco/data-pull/addon/pipelines/home)
* validate Vagrant, css, etc.
* add staging, production
